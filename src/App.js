import logo from './logo.svg';
import './App.css';
import Game from './components/game/Game.js';

function App() {
  return (
    <div className="App">
      <Game location="Amway Center"/>
    </div>
  );
}

export default App;
