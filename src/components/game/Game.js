import Team from '../../components/team/Team.js';
import React from 'react';

class Game extends React.Component{
    render(){
        return <React.Fragment>
                   <h1 id="title">Welcome to <br/>
                       <span>{this.props.location}</span>
                   </h1>
                   <div id="teamContainer">
                   <Team name="Clippers" logo="https://wallpapercave.com/wp/wp1830197.jpg"/>
                   <Team name="Pacers" logo="https://wallpapercave.com/wp/wp1878775.jpg"/>
                   </div>
               </React.Fragment>;
    }
   }
   export default Game;