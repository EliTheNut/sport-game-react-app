
import swish from './Swish.wav';
import React from 'react';

let scoreSound = new Audio(swish);

class Team extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            shotsTaken: 0,
            score: 0,
            accuracy: 0
        };
        this.shoot = this.shoot.bind(this);
    }
    shoot(event){
        
        let chance = Math.floor(Math.random() * 100);
        this.setState((state) => ({
            shotsTaken: state.shotsTaken + 1
        }));
        if(chance < 75){
            this.setState((state) => ({
                score: state.score + 2
                
            }));
            scoreSound.play();
        }
        if(this.state.score > 0 && this.state.shotsTaken > 0){
            this.setState((state) => ({
                accuracy: (state.shotsTaken / (state.score / 2)).toFixed(2)
            }));
            
        }
    }
    render(){
        return <React.Fragment>
                    <div id={this.props.name} class="team">
                        <img class="logo" src={this.props.logo}/>
                        <h1>{this.props.name}</h1>
                        <div id="stats">
                            <div class="statBox">
                            <p>{this.state.shotsTaken}</p>
                            <br/>
                            <p>Shots</p></div>
                            <div class="statBox">
                            <p>{this.state.score}</p>
                            <br/>
                            <p>Score</p></div>
                            <div class="statBox">
                            <p>{this.state.accuracy}</p>
                            <br/>
                            <p>Accuracy</p></div>
                        </div>
                        <button onClick={this.shoot}>Shoot</button>
                    </div>
                </React.Fragment>;
    }
}
export default Team;